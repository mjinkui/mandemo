<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
div#container{width:500px}
div#header {background-color:#99bbbb;}
div#menu {background-color:#ffff99;height:200px;width:100px;float:left;}
div#content {background-color:#EEEEEE;height:200px;width:400px;float:left;}
div#footer {background-color:#99bbbb;clear:both;text-align:center;}
h1 {margin-bottom:0;}
h2 {margin-bottom:0;font-size:18px;}
ul {margin:0;}
li {list-style:none;}
</style>
</head>

<body>

<div id="container">

<div id="header">
<h1>Main Title of Web Page</h1>
</div>

<div id="menu">
<h2>Menu</h2>
<ul>
<li>HTML</li>
<li>CSS</li>
<li>JavaScript</li>
</ul>
</div>

<div id="content">Content goes here
<% String datetime1 = (String)request.getParameter("name"); out.println(datetime1);%>
<% String province= (String)request.getParameter("province"); out.println("你提交的省是:"+province);%>
<% String city= (String)request.getParameter("city"); out.println("你提交的市是:"+city);%>
<% String county= (String)request.getParameter("county"); out.println("你提交的区县是:"+county);%>
<% String town= (String)request.getParameter("town"); out.println("你提交的乡镇是:"+town);%>
<% String village= (String)request.getParameter("village"); out.println("你提交的村是:"+village);%>
</div>

<div id="footer">Copyright W3School.com.cn</div>

</div>

</body>
</html>
